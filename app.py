"""SDNC mock application."""
from flask import Flask
from flask_restful import Api

from resources.preload import Preload


app = Flask(__name__)
api = Api(app)


api.add_resource(
    Preload,
    "/restconf/operations/GENERIC-RESOURCE-API:preload-vf-module-topology-operation",
    "/restconf/operations/GENERIC-RESOURCE-API:preload-network-topology-operation",
)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
