"""SDNC preload resource module."""
from flask_restful import Resource


class Preload(Resource):
    """Preload resource class."""

    def post(self) -> None:
        """Preload resource mock method.

        Do nothing.

        """
